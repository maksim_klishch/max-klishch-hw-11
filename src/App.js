import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import logo from './logo.svg';
import './App.css';

import { fetchProducts } from "./app/API/productsAPI"

//import components
import Store from "./components/Store"
import Cart from './components/Cart';
import Modal from 'react-modal';


Modal.setAppElement("#root")

function App() {
  const dispatch = useDispatch()

  useEffect(() => {
    fetchProducts()(dispatch)
  }, [dispatch])

  return (
    <div className="App">
      <Cart />
      <Store />
    </div>
  );
}

export default App;
