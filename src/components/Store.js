import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button, Card, 
         CardActions, 
         CardContent, 
         CardMedia, 
         Grid,
         TextField, 
         Typography, 
         FormControl} from "@material-ui/core";
import { addToCart } from '../app/actions';

function Store() {
    const {
        products
    } = useSelector((state) => state.productsManager);

    const dispatch = useDispatch()

    const [productsCount, setProductsCount] = useState(Array(products.length).fill(1))

    useEffect(() => {
        setProductsCount(Array(products.length).fill(1))
    }, [products])

    const handleOnTextFieldChange = (product) => (e) => {
        productsCount[product.id - 1] = e.target.value
    }

    const handleAddToCart = (product) => () => {
        dispatch(addToCart({
            count: +productsCount[product.id - 1], 
            productId: product.id
        }))
    }

    return (
        <Grid container spacing={3}>
            {!!products.length &&
                products.map(product => 
                    <Grid item key={product.id} xs={2}>
                        <Card>
                            <CardMedia 
                                component="img"
                                image={product.image}
                                height={150} />
                            <CardContent>
                                <Typography noWrap>{product.title}</Typography>
                                <Typography>{product.price + "$"}</Typography>
                            </CardContent>
                            <CardActions>
                                <FormControl>
                                    <TextField
                                        id={"product-count-" + product.id} 
                                        inputProps = {{min: 1}}
                                        label="How many?" 
                                        type="number" 
                                        defaultValue={1}
                                        onChange={handleOnTextFieldChange(product)}/>
                                    <Button 
                                        size="small" 
                                        color="primary" 
                                        onClick={handleAddToCart(product)}>
                                        Add to cart
                                    </Button>
                                </FormControl>
                            </CardActions>
                        </Card>
                    </Grid>)}
        </Grid>
    );
}

export default Store