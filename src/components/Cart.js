import { Box, 
         IconButton, 
         Table, 
         TableBody, 
         TableCell, 
         TableRow, 
         Typography } from "@material-ui/core";
import { ShoppingCart, AddCircle, RemoveCircle, Delete } from "@material-ui/icons";
import { useState } from "react";
import Modal from "react-modal"
import { useDispatch, useSelector } from "react-redux";
import { addToCart, removeProductFromCart } from "../app/actions";

const styles = {
    cartFixed: {
        position: 'fixed', 
        top: 30, 
        right: 10, 
        overflowY: 'scroll'
    },

    icon: {
        backgroundColor: "#dddddd"
    }
}

function Cart() {
    const {
        products,
        cart
    } = useSelector((state) => state.productsManager);

    const dispatch = useDispatch()

    const [isModalOpen, setIsModalOpen] = useState(false)


    function openModal() {
        setIsModalOpen(true)
    }

    function closeModal() {
        setIsModalOpen(false)
    }

    const handleRemoveProduct = (productId) => () => {
        dispatch(removeProductFromCart({productId}))
    }

    const handleAddItem = (productId) => () => {
        dispatch(addToCart({
            productId: productId, 
            count: 1
        }))
    }

    const handleRemoveItem = (productId) => () => {
        dispatch(addToCart({
            productId: productId, 
            count: -1
        }))
    }

    return (
        <>
            <Box align="right" style={styles.cartFixed}>
                <IconButton 
                    color="primary" 
                    style={styles.icon} 
                    onClick = {openModal}>
                        <ShoppingCart />
                </IconButton>
            </Box>
            <Modal
                isOpen={isModalOpen}
                onRequestClose = {closeModal}
                contentLabel="Cart">

                <h1>Cart</h1>
            
                <Table>
                    <TableBody>
                        {!!cart.length && cart.map(item => {
                                const product = products.find(product => product.id === item.productId)
                                return (
                                    <TableRow key={product.id}>
                                        <TableCell>
                                            <Typography>{product.title}</Typography>
                                        </TableCell>
                                        <TableCell>
                                            <IconButton onClick={handleRemoveItem(product.id)}>
                                                <RemoveCircle />
                                            </IconButton>
                                            {item.count}
                                            <IconButton onClick={handleAddItem(product.id)}>
                                                <AddCircle />
                                            </IconButton>
                                        </TableCell>
                                        <TableCell>
                                            {(product.price * item.count).toFixed(2)}$
                                        </TableCell>
                                        <TableCell>
                                            <IconButton onClick={handleRemoveProduct(product.id)}>
                                                <Delete />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                )
                            }
                        )}
                    </TableBody>
                </Table>

            </Modal>
        </>
    )
}

export default Cart