import {
    PRODUCTS_MANAGER_ADD_TO_CART,
    PRODUCTS_MANAGER_SET_CART,
    PRODUCTS_MANAGER_SET_PRODUCTS,
    PRODUCTS_MANAGER_REMOVE_PRODUCT_FROM_CART
} from "../actions"

const initialState = {
    products: [],
    cart: []
}

const productsManager = (state = initialState, {type, payload}) => {
    switch(type){
        case PRODUCTS_MANAGER_SET_PRODUCTS:
            return {
                ...state,
                products: payload
            }

        case PRODUCTS_MANAGER_SET_CART:
            return {
                ...state,
                cart: payload
            }

        case PRODUCTS_MANAGER_ADD_TO_CART:
        {
            const productIndex = state.cart.findIndex(
                item => item.hasOwnProperty('productId') 
                && item.productId === payload.productId)

            if (productIndex !== -1)
            {
                state.cart[productIndex].count += payload.count

                if(state.cart[productIndex].count <= 0) state.cart.splice(productIndex, 1)

                return {...state}
            }

            return {
                ...state,
                cart: [...state.cart, payload]
            }
        }

        case PRODUCTS_MANAGER_REMOVE_PRODUCT_FROM_CART:
        {
            const productIndex = state.cart.findIndex(
                item => item.hasOwnProperty('productId') 
                && item.productId === payload.productId)

            state.cart.splice(productIndex, 1)
            return {...state}
        }

        default:
            return state
    }
}

export default productsManager
