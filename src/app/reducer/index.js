import { combineReducers } from "redux";
import productsManagerReducer from "./productsManager";

const rootReducer = combineReducers({
    productsManager: productsManagerReducer
});

export default rootReducer;