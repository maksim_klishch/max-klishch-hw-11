export const PRODUCTS_MANAGER_SET_PRODUCTS = "ProductManager/SET_PRODUCTS"
export const PRODUCTS_MANAGER_SET_CART = "ProductManager/SET_CART"
export const PRODUCTS_MANAGER_ADD_TO_CART = "ProductManager/ADD_TO_CART"
export const PRODUCTS_MANAGER_REMOVE_PRODUCT_FROM_CART = "ProductManager/REMOVE_PRODUCT_FROM_CART"

export const setProducts = (payload) => ({
    type: PRODUCTS_MANAGER_SET_PRODUCTS,
    payload
})

export const setCart = (payload) => ({
    type: PRODUCTS_MANAGER_SET_CART,
    payload
})

export const addToCart = (payload) => ({
    type: PRODUCTS_MANAGER_ADD_TO_CART,
    payload
})

export const removeProductFromCart = (payload) => ({
    type: PRODUCTS_MANAGER_REMOVE_PRODUCT_FROM_CART,
    payload
})