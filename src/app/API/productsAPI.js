import { setProducts } from "../actions"
import { API_PRODUCTS_URI } from "../../consts"

export const fetchProducts = () => (dispatch) => {
    fetch(API_PRODUCTS_URI)
        .then(response => response.json())
        .then(data => dispatch(setProducts(data)))
}